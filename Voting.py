'''
In case we cannot discuss, this is a to-do list:
- Change the name input method to accept 2, then to allow breaks with the string "Done"
- Create the dictionary "votes" with a key for each value in "candidates"
- Print the list of candidates with the corresponding number to vote for them
- Take the input number and use it to increment its corresponding key
- Sort the votes
- Take the key with the highest votes and use it to access its corresponding value in the candidates list. Print this value
- Print the results
'''

# Start by initializing all the variables that we are going to use
count = 0
candidates = []
votes = {}

# Accept names of the candidates (2 to 4), and insert it into a list
for x in range(0,4):
    candidates.append(input("Enter name of candidate: "))

# Create a key for each candidate


# Start voting


# Sort votes and declare winner
winner = sorted(candidates, key=candidates.__getitem__)[0]
print("The winner is", candidates[winner])

# Print the results