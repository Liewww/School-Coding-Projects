# These are Boolean expressions which result in a value of true or false
# Note that Python stores true as integer 1, and false as interger 0, but outputs 'true' or 'false' from print statements

print(7 > 10)
print(4 < 16)
print(4 == 4)
print(4 <= 4)
print(4 >= 4)
print(4 != 4)