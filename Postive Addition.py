total = 0

for x in range(0,10):
    number = int(input("Input a positive number: "))

    while number < 0:
        number = int(input("Invalid value. Please enter a positive number: "))
    
    total += number

print("The total is:", total)
print("The average is:", total / 10)
