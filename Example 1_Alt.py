for a in reversed(range(1,11)):
    print (a)
    if a > 5:
        print("Big number!")
    elif a % 2 != 0:
        print("This is an odd number")
        print("It isn't greater than five, either")
    else:
        print("This number isn't greater than 5")
        print("Nor is it odd")
        print("Feeling special?")
    print("We just made 'a' one less than what it was!")
    print("And unless 'a' is not greater that 0, we'll do the loop again.")

print("Well, it seems as if 'a' is now no longer bigger than 0!")
print("The loop is now over, and without further ado, so is this program!")