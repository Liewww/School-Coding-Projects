total = 0
count = 0

while True:
    number = int(input("Input a positive number, or enter -1 to exit: "))

    if number == -1:
        break
    elif number < 0:
        print("Invalid number")
        continue

    total += number
    count += 1

print("The total is:", total)
print("The average is:", total / count)