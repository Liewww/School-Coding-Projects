score = input("Enter score: ")
score = int(score)

if score >= 80:
    grade = "A"
else:
    if score >= 70:
        grade = "B"
    else:
        if score >= 55:
            grade = "C"
        else:
            if score >= 50:
                grade = "Pass"
            else:
                grade = "Fail"

print("\n\nGrade is:" , grade)