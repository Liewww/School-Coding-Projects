marks = int(input("Input marks: "))

if marks < 0 or marks > 20:
    print("Invalid mark")
else:
    if marks >= 10:
        print("Pass")
    else:
        print("Fail")
